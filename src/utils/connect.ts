import mongoose from "mongoose";
import config from "config";
import logger from "./logger";

async function connect() {
  // const dbUri = config.get<string>("dbUri");
  const dbUri = process.env.DB_CONNECTION as string;

  try {
    await mongoose.connect(dbUri);
    logger.info("dbURI", process.env.DB_CONNECTION);
    logger.info("DB connected", dbUri);
  } catch (error) {
    console.log(error);
    logger.error("Could not connect to db");
    process.exit(1);
  }
}

export default connect;
