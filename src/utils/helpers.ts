import { result } from "lodash";

export function getValueOfObjectArray(
  arr: Array<any>,
  keyToGet: string,
  keyName: string,
  valueName: string
) {
  /*  get specific value of object form : 
  [
  _id:""
  _value:""
  ]
  */
  console.log("param", arr, keyToGet, keyName, valueName);

  let result = 0;

  arr.every((element) => {
    console.log(
      element,
      keyToGet,
      element[keyName],
      valueName,
      keyToGet === element[keyName]
    );
    if (keyToGet === element[keyName]) {
      result = element[valueName];
      return false;
    }
    return true;
  });

  return result;
}

export function convertTimestampToDate2(...agrs: number[]): string[] {
  let result: string[] = [];
  agrs.forEach((param) => {
    const date = new Date(param).toLocaleDateString("vi-VN");
    result.push(date);
  });
  return result;
}
export function convertTimestampToDate(...agrs: number[]): string[] {
  let result: string[] = [];
  agrs.forEach((param) => {
    // const date = new Date(param).toLocaleDateString("vi-VN");
    if (!param) {
      result.push("-");
    } else {
      // const date = new Date(param);
      // const day = date.getDate();
      // const month = date.getMonth()+1;
      // const year = date.getFullYear();
      // result.push(`${day}-${month}-${year}`);
      const today = new Date(param);
      const yyyy = today.getFullYear();
      let mm:any = today.getMonth() + 1; // Months start at 0!
      let dd:any = today.getDate();

      if (dd < 10) dd = "0" + dd;
      if (mm < 10) mm = "0" + mm;

      let res = dd + "-" + mm + "-" + yyyy;
      result.push(res);
    }
  });
  return result;
}

export function arrayStringToParagraph(arr: string[]): string {
  let res = "";
  try {
    arr.forEach((element) => {
      res += element += "\n";
    });
  } catch (error) {
    console.log("error convert array string:");
  } finally {
    return res;
  }
}
