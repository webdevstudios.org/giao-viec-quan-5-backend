import config from "config";
import cors from "cors";
import dotenv from "dotenv";
import express, { Request, Response } from "express";
import responseTime from "response-time";
import deserializeUser from "./middleware/deserializeUser";
import routes from "./old_routes";
import BaseRouter from "./routes";
import { createFirstAdmin } from "./service/admin.service";
import connect from "./utils/connect";
import logger from "./utils/logger";
import { restResponseTimeHistogram, startMetricsServer } from "./utils/metrics";
import swaggerDocs from "./utils/swagger";
dotenv.config();

const port = process.env.PORT;
const dbURI = process.env.DB_CONNECTION;

const app = express();

// app.use(cors());

// var whitelist = ["http://localhost:3000", "http://localhost:3001"];
app.use(
  cors({
    origin: (origin, callback) => {
      callback(null, true);
    },
  })
);

app.use(express.json());

app.use(deserializeUser);

app.use(
  responseTime((req: Request, res: Response, time: number) => {
    if (req?.route?.path) {
      restResponseTimeHistogram.observe(
        {
          method: req.method,
          route: req.route.path,
          status_code: res.statusCode,
        },
        time * 1000
      );
    }
  })
);

app.use("/api/quan5", BaseRouter);

app.listen(port, async () => {
  logger.info(`App is running at http://localhost:${port}`);
  logger.info(`URI - ${dbURI}`);

  await connect().then(() => createFirstAdmin()); // kết nối mongodb

  startMetricsServer();

  swaggerDocs(app, parseInt(port || "3000"));
});
