import { Router } from "express";
import {
  createVanBanHandler,
  deleteVanBanHandler,
  getVanBanById,
  getVanBansHandler,
  updateVanBanHandler,
} from "../controller/vanban.controller";
import {
  requireActiveAdmin,
  requireAdminUser,
  requireMasterAdmin,
} from "../middleware/validateAdmin";

const vanBanRouter = Router();

vanBanRouter.get(
  "/all",
  [requireAdminUser, requireActiveAdmin],
  getVanBansHandler
);

vanBanRouter.post(
  "/create",
  [requireActiveAdmin, requireAdminUser],
  createVanBanHandler
);

vanBanRouter.patch(
  "/:vanBanId",
  [requireAdminUser, requireActiveAdmin],
  updateVanBanHandler
);
vanBanRouter.get(
  "/:vanBanId",
  [requireAdminUser, requireActiveAdmin],
  getVanBanById
);

vanBanRouter.delete(
  "/:vanBanId",
  [requireMasterAdmin, requireActiveAdmin],
  deleteVanBanHandler
);

export default vanBanRouter;
