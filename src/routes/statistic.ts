import { Router } from "express";
import { exportExcelHandler, getOverviewStatisticHandler } from "../controller/statistic.controller";
import {
  requireActiveAdmin,
  requireAdminUser,
} from "../middleware/validateAdmin";

const statisticRouter = Router();

statisticRouter.get(
  "/overall",
  [requireActiveAdmin, requireAdminUser],
  getOverviewStatisticHandler
);
statisticRouter.get(
  "/export-excel",
  // [requireActiveAdmin, requireAdminUser],
  exportExcelHandler
);

export default statisticRouter;
