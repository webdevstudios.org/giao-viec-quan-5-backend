import { Router } from "express";
import { adminLoginHandler } from "../controller/auth.controller";
import validateResource from "../middleware/validateResource";
import { adminLoginSchema } from "../schema/admin.schema";

const authRouter = Router();

authRouter.post("/login",validateResource(adminLoginSchema),adminLoginHandler);

export default authRouter;