import { Express, Router } from "express";
import { createAdminHandler, deleteAdminHandler, getAllAdminsHandler, updateAdminHandler } from "../controller/admin.controller";
import { requireMasterAdmin } from "../middleware/validateAdmin";
import validateResource from "../middleware/validateResource";
import { createAdminSchema, deleteAdminSchema, updateAdminSchema } from "../schema/admin.schema";

const adminRouter = Router();

adminRouter.post(
  "/create",
  requireMasterAdmin,
  validateResource(createAdminSchema),
  createAdminHandler
);
adminRouter.get(
  "/all",
  requireMasterAdmin,
  getAllAdminsHandler
);

adminRouter.delete(
  "/:adminId",
  [requireMasterAdmin,validateResource(deleteAdminSchema)],
  deleteAdminHandler
)
adminRouter.patch(
  "/:adminId",
  [requireMasterAdmin,validateResource(updateAdminSchema)],
  updateAdminHandler
)

export default adminRouter;
