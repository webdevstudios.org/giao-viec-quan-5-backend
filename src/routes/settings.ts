import { Router } from "express";
import { getAllEnumsHandler } from "../controller/settings.controller";


const settingsRouter = Router();

settingsRouter.get("/all-enums",getAllEnumsHandler);

export default settingsRouter;