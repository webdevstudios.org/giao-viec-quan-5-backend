import { Express, Router } from "express";
import {
  createDonViHandler,
  deleteDonViHandler,
  getAllDonViHandler,
  helloDonVi,
  updateDonViHandler,
} from "../controller/donVi.controller";
import {
  requireAdminUser,
  requireActiveAdmin,
  requireMasterAdmin,
} from "../middleware/validateAdmin";

const donViRouter = Router();

donViRouter.post(
  "/create",
  [requireActiveAdmin, requireAdminUser],
  createDonViHandler
);
donViRouter.patch(
  "/:id",
  [requireActiveAdmin, requireAdminUser],
  updateDonViHandler
);
donViRouter.get(
  "/all",
  [requireActiveAdmin, requireAdminUser],
  getAllDonViHandler
);
donViRouter.delete(
  "/:id",
  [requireMasterAdmin, requireAdminUser],
  deleteDonViHandler
);

export default donViRouter;
