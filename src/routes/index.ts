import { Router } from "express";
import { helloDonVi } from "../controller/donVi.controller";
import adminRouter from "./admin";
import authRouter from "./auth";
import donViRouter from "./donVi";
import settingsRouter from "./settings";
import statisticRouter from "./statistic";
import vanBanRouter from "./vanban";

const baseRouter = Router();

baseRouter.get("/", helloDonVi);

baseRouter.use("/don_vi", donViRouter);
baseRouter.use("/admin", adminRouter);
baseRouter.use("/auth", authRouter);
baseRouter.use("/settings", settingsRouter);
baseRouter.use("/van_ban", vanBanRouter);
baseRouter.use("/statistic", statisticRouter);

export default baseRouter;
