import Joi from "joi";

export const createDonViSchema = Joi.object({
  name: Joi.string().required(),
  code: Joi.string().allow(),
  description: Joi.string().allow(),
});
