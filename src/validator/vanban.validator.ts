import Joi, { string } from "joi";
import {
  LoaiDon,
  LoaiVanBan,
  TinhTrangXuLyVanBanDon,
  TinhTrangXuLyVanBanHanhChinh,
} from "../shared/constants";

const { ChuanBiTrienKhai, DaHoanThanh, DaHuy, DangThucHien } =
  TinhTrangXuLyVanBanHanhChinh;
const { DaGiaiQuyet, DangGiaiQuyet, Tre } = TinhTrangXuLyVanBanDon;
const { KhieuNai, PhanAnh, ToCao } = LoaiDon;

export const getVanBanQuerySchema = Joi.object({
  pageNo: Joi.number(),
  size: Joi.number(),
  loaiVanBan: Joi.string().valid(
    null,
    LoaiVanBan.HanhChinh,
    LoaiVanBan.VanBanDon
  ),
  tinhTrangXuLyVanBanHanhChinh: Joi.string().valid(
    null,
    ChuanBiTrienKhai,
    DaHoanThanh,
    DaHuy,
    DangThucHien
  ),
  tinhTrangXuLyVanBanDon: Joi.string().valid(
    null,
    DangGiaiQuyet,
    Tre,
    DaGiaiQuyet,
    TinhTrangXuLyVanBanDon.DaHuy
  ),
  search: Joi.string().allow(""),
  loaiDon: Joi.string().valid(null, KhieuNai, PhanAnh, ToCao),
  donViId: Joi.string().allow(""),
});

export const createVanBanSchema = Joi.object({
  tenVanBan: Joi.string(),
}).unknown(true);

export const updateVanBanSchema = Joi.object({}).unknown(true);

/*

"donVi": "",
"tenVanBan": "",
"loaiVanBan": "",
"soKyHieu": "",
"ngayBatDau": "",
"nguoiChiDao": "",
"chuyenVienTheoDoi": "",
"thoiHan": "",
"trichYeu": "",
"ghiChu": "",
"tinhTrangXuLyVanBanHanhChinh": "",

"loaiDon": "",
"thongTinNguoiGui": "",
"noiDungDon": "",
"huongXuLy": "",
"noiDungVanBanPhanHoi": "",
"tinhTrangXuLyVanBanDon": ""

*/
