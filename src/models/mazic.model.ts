import mongoose from "mongoose";
import { PresentDocument } from "./present.model";

export interface MazicInput {
  presentId: PresentDocument["_id"];
  spinningCode: string;
}

export interface MazicDocument extends MazicInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const MazicSchema = new mongoose.Schema({
  presentId: { type: mongoose.Schema.Types.ObjectId, ref: "Present" },
  spinningCode: { type: String, required: true },
});

const MazicModel = mongoose.model<MazicDocument>("Mazic", MazicSchema);

export default MazicModel;
