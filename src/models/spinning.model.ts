import mongoose from "mongoose";
import { customAlphabet } from "nanoid";
import { PresentDocument } from "./present.model";
import crypto from "crypto";

const nanoid = customAlphabet("abcdefghijklmnopqrstuvwxyz0123456789", 10);
const pre_spinning_code = customAlphabet("ABCDEFGHIKLMNOQRSTUVWXYZ", 2);
const suf_spinning_code = customAlphabet("12345678980", 4);

export interface SpinningInput {
  presentId: PresentDocument["_id"];
  clientFullname: string;
  code: string;
  available: boolean;
  brand: string;
}

export interface SpinningDocument extends SpinningInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const spinningSchema = new mongoose.Schema(
  {
    spinningId: {
      type: String,
      required: true,
      unique: true,
      default: () => `spinning_${nanoid()}`,
    },
    presentId: { type: mongoose.Schema.Types.ObjectId, ref: "Present" },
    clientFullname: { type: String, required: false },
    brand: { type: String, required: false },
    code: {
      type: String,
      required: true,
      unique: true,
      // default: () => crypto.randomBytes(4).toString("hex").toUpperCase(),
      default: () => `${pre_spinning_code()}${suf_spinning_code()}`
    },
    available: { type: Boolean, required: true, default: () => true },
  },
  {
    timestamps: true,
  }
);

const SpinningModel = mongoose.model<SpinningDocument>(
  "Spinning",
  spinningSchema
);

export default SpinningModel;
