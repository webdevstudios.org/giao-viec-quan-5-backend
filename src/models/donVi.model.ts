import mongoose from "mongoose";

export interface DonViAttributes {
  name: string;
  description?: string;
  code?: string;
  isDeleted?: boolean;
}

export interface DonViDocument extends DonViAttributes, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const DonViSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  code: { type: String},
  isDeleted: { type: Boolean, required: true, default: () => false },
});

const DonViModel = mongoose.model<DonViDocument>("DonVi", DonViSchema);

export default DonViModel;
