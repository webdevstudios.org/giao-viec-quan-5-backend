import mongoose from "mongoose";
import { customAlphabet } from "nanoid";

const nanoid = customAlphabet("abcdefghijklmnopqrstuvwxyz0123456789", 10);

export interface PresentInput {
  name: string;
  image: string;
  number: number;
  status: boolean;
  code:string;
}

export interface PresentDocument extends PresentInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const presentSchema = new mongoose.Schema(
  {
    presentId: {
      type: String,
      required: true,
      unique: true,
      default: () => `present_${nanoid()}`,
    },
    name: { type: String, required: true },
    image: { type: String, required: true },
    code: { type: String, required: true },
    number: { type: Number, required: true },
    status: { type: Boolean, required: true, default: () => false },
  },
  {
    timestamps: true,
  }
);

const PresentModel = mongoose.model<PresentDocument>("Present", presentSchema);

export default PresentModel;
