import mongoose from "mongoose";
import { Quan5Enums } from "../shared/constants";
import { AdminDocument } from "./admin.model";
import { DonViDocument } from "./donVi.model";

export interface FilterVanBanSchema {
  pageNo: number;
  size: number;
  loaiVanBan: string;
  tinhTrangXuLyVanBanHanhChinh: string;
  tinhTrangXuLyVanBanDon: string;
  loaiDon: string;
  search: string;
  donViId: string;
}

export interface VanBanAttributes {
  adminId: AdminDocument["_id"];
  loaiVanBan: string;

  tenVanBan: string;
  coQuanBanHanh: string;

  soKyHieu: string;
  ngayBanHanh: number;

  nguoiChiDao: string;
  noiDungChiDao: string;

  donViId: DonViDocument["_id"];
  ngayBatDau: number;
  thoiHan: number;
  chuyenVienTheoDoi: string;
  tinhTrangXuLyVanBanHanhChinh: string;

  ghiChu: string;
  noiDungVanBanHanhChinh: string;

  //----- van ban don --------------------

  // tên đơn == tên văn bản
  loaiDon: string;
  ngayNhanDon:number;
  noiDungDon: string;

  //---thông tin người gởi đơn
  hoTenNguoiGui: string;
  diaChiNguoiGui: string;
  sdtNguoiGui: string;
  cccdNguoiGui: string;

  //nội dung phản hồi
  noiDungVanBanPhanHoi: string;
  huongXuLy: string;
  tinhTrangXuLyVanBanDon: string;

  // ghi chú

  isDeleted: boolean;
}

export interface VanBanDocument extends VanBanAttributes, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const VanBanSchema = new mongoose.Schema({
  donViId: { type: mongoose.Schema.Types.ObjectId, ref: "DonVi" },
  adminId: { type: mongoose.Schema.Types.ObjectId, ref: "Admin" },
  loaiVanBan: { type: String, enum: Quan5Enums.LoaiVanBan },

  tenVanBan: { type: String },
  coQuanBanHanh: { type: String },

  soKyHieu: { type: String },
  ngayBanHanh: { type: Number },

  nguoiChiDao: { type: String },
  noiDungChiDao: { type: String },

  ngayBatDau: { type: Number, default: new Date(Date.now()).getTime() },
  thoiHan: { type: Number, required: false },
  chuyenVienTheoDoi: { type: String },
  tinhTrangXuLyVanBanHanhChinh: {
    type: String,
    enum: [...Quan5Enums.TinhTrangXuLyVanBanHanhChinh, ""],
  },

  ghiChu: { type: String },
  noiDungVanBanHanhChinh: { type: String },

  //văn bản đơn

  loaiDon: { type: String, enum: [...Quan5Enums.LoaiDon, ""] },
  ngayNhanDon: { type: Number, default: new Date(Date.now()).getTime() },
  noiDungDon: { type: String },

  hoTenNguoiGui: { type: String },
  diaChiNguoiGui: { type: String },
  sdtNguoiGui: { type: String },
  cccdNguoiGui: { type: String },

  noiDungVanBanPhanHoi: { type: String },
  huongXuLy: { type: String },
  tinhTrangXuLyVanBanDon: {
    type: String,
    enum: [...Quan5Enums.TinhTrangXuLyVanBanDon, ""],
  },

  isDeleted: { type: Boolean, required: true, default: () => false },

});

VanBanSchema.index({
  "tenVanBan": "text",
  "coQuanBanHanh":"text",

  "soKyHieu": "text",
  "ngayBanHanh": "text",

  "nguoiChiDao":"text",
  "noiDungChiDao":"text",

  "ngayBatDau":"text",
  "thoiHan":"text",

  "chuyenVienTheoDoi":"text",
  "tinhTrangXuLyVanBanHanhChinh":"text",

  "ghiChu":"text",
  "noiDungVanBanHanhChinh":"text",

  "loaiDon":"text",
  "noiDungDon":"text",

  "hoTenNguoiGui": "text",
  "diaChiNguoiGui":"text",
  "sdtNguoiGui":"text",
  "cccdNguoiGui":"text",

  "noiDungVanBanPhanHoi":"text",
  "huongXuLy":"text",
  "tinhTrangXuLyVanBanDon":"text"
});

const VanBanModel = mongoose.model<VanBanDocument>("VanBan", VanBanSchema);

export default VanBanModel;
