import mongoose from "mongoose";
import bcrypt from "bcrypt";
import config from "config";
import * as z from "zod";

export enum AdminRole {
  admin,
  master_admin,
}

export interface AdminInput {
  username: string;
  fullname: string;
  password: string;
  role: "admin" | "master_admin";
  isActive?: boolean;
  isDeleted?:boolean,
}

export interface AdminDocument extends AdminInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
  comparePassword(candidatePassword: string): Promise<Boolean>;
}

const adminSchema = new mongoose.Schema(
  {
    username: { type: String, required: true, unique: true },
    fullname: { type: String, required: true },
    password: { type: String, required: true },
    role: { type: String, required: true, enum: ["admin", "master_admin"] },
    isActive: { type: Boolean, required: true, default: () => true },
    isDeleted: { type: Boolean, required: true, default: () => false },
  },
  {
    timestamps: true,
  }
);

adminSchema.pre("save", async function (next) {
  let admin = this as AdminDocument;

  if (!admin.isModified("password")) {
    return next();
  }

  const salt = await bcrypt.genSalt(config.get<number>("saltWorkFactor"));

  const hash = await bcrypt.hashSync(admin.password, salt);

  admin.password = hash;

  return next();
});

adminSchema.methods.comparePassword = async function (
  candidatePassword: string
): Promise<boolean> {
  const admin = this as AdminDocument;

  return bcrypt.compare(candidatePassword, admin.password).catch((e) => false);
};

const AdminModel = mongoose.model<AdminDocument>("Admin", adminSchema);

export default AdminModel;
