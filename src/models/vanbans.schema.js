const  VanBanAttributes = {
    adminId: "id",
    loaiVanBan: "chuoi",
  
    tenVanBan: "chuoi",
    coQuanBanHanh: "chuoi",
  
    soKyHieu: "chuoi",
    ngayBanHanh: 0,
  
    nguoiChiDao: "chuoi",
    noiDungChiDao: "chuoi",
  
    donViId: "id",
    ngayBatDau: 0,
    thoiHan: 0,
    chuyenVienTheoDoi: "chuoi",
    tinhTrangXuLyVanBanHanhChinh: "chuoi",
  
    ghiChu: "chuoi",
    noiDungVanBanHanhChinh: "chuoi",
  
    //----- van ban don --------------------
  
    // tên đơn == tên văn bản
    loaiDon: "chuoi",
    //ngày nhận đơn == ngày bắt đầu
    noiDungDon: "chuoi",
  
    //---thông tin người gởi đơn
    hoTenNguoiGui: "chuoi",
    diaChiNguoiGui: "chuoi",
    sdtNguoiGui: "chuoi",
    cccdNguoiGui: "chuoi",
  
    //nội dung phản hồi
    noiDungVanBanPhanHoi: "chuoi",
    huongXuLy: "chuoi",
    tinhTrangXuLyVanBanDon: "chuoi",
  
    // ghi chú
  
    isDeleted: false,
  }

  //json
  const vanban =
  {
    "adminId": "id",
    "loaiVanBan": "chuoi",
    "tenVanBan": "chuoi",
    "coQuanBanHanh": "chuoi",
    "soKyHieu": "chuoi",
    "ngayBanHanh": 0,
    "nguoiChiDao": "chuoi",
    "noiDungChiDao": "chuoi",
    "donViId": "id",
    "ngayBatDau": 0,
    "thoiHan": 0,
    "chuyenVienTheoDoi": "chuoi",
    "tinhTrangXuLyVanBanHanhChinh": "chuoi",
    "ghiChu": "chuoi",
    "noiDungVanBanHanhChinh": "chuoi",
    "loaiDon": "chuoi",
    "noiDungDon": "chuoi",
    "hoTenNguoiGui": "chuoi",
    "diaChiNguoiGui": "chuoi",
    "sdtNguoiGui": "chuoi",
    "cccdNguoiGui": "chuoi",
    "noiDungVanBanPhanHoi": "chuoi",
    "huongXuLy": "chuoi",
    "tinhTrangXuLyVanBanDon": "chuoi",
    "isDeleted": false
  }