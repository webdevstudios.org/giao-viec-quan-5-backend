import { object, string, TypeOf } from "zod";

export const createMazicInput = object({
  body: object({
    presentId: string({ required_error: "presentId is required" }),
    spinningCode: string({ required_error: "spinningCode is required" }),
  }),
});

export type CreateMazicInput = TypeOf<typeof createMazicInput>;