import { boolean, number, object, optional, string, TypeOf } from "zod";
import { PresentInput } from "../models/present.model";

/**
 * @openapi
 * components:
 *  schemas:
 *    CreatePresentInput:
 *      type: object
 *      required:
 *        - name
 *        - image
 *        - number
 *        - status
 *      properties:
 *        name:
 *          type: string
 *          default: iphone13
 *        image:
 *          type: string
 *          default: linkurl
 *        number:
 *          type: number
 *          default: 500
 *        status:
 *          type: boolean
 *          default: true
 *    CreatePresentResponse:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        image:
 *          type: string
 *        number:
 *          type: number
 *        status:
 *          type: boolean
 *        createdAt:
 *          type: string
 *        updatedAt:
 *          type: string
 */

// export interface CreatePresentInput extends PresentInput {

// };

export const createPresentInput = object({
    body: object({
      name: string({ required_error: "name is required" }),
      image: string({ required_error: "image is required" }),
      number: number({ required_error: "number is required" }),
      code: string({ required_error: "code is required" }),
      status: boolean({ required_error: "status is required" }),
    }),
  });
  
  export type CreatePresentInput = TypeOf<typeof createPresentInput>;

