import { object, optional, string, TypeOf } from "zod";
import { SpinningInput } from "../models/spinning.model";


/**
 * @openapi
 * components:
 *  schemas:
 *    SpinTheWheelInput:
 *      type: object
 *      required:
 *        - code
 *        - brand
 *        - clientFullname
 *      properties:
 *        code:
 *          type: string
 *          default: ""
 *        brand:
 *          type: string
 *          default: Hashaki
 *        clientFullname:
 *          type: string
 *          default: NgoVanPhong
 *    SpinTheWheelResponse:
 *      type: object
 *      properties:
 */

export interface CreateSpinningInput extends SpinningInput {

}

const spinTheWheelPayload = {
    body:object({
        code: string({required_error:"code is required"}),
        brand:string({required_error:"brand is required"}),
        clientFullname:optional(string())
    })
}

export const spinTheWheelSchema = object({
    ...spinTheWheelPayload
})

export type SpinTheWheelInput = TypeOf<typeof spinTheWheelSchema>;