import { boolean, object, optional, string, TypeOf } from "zod";
import * as z from "zod";

export const createAdminSchema = object({
  body: object({
    username: string({
      required_error: "username is required",
    }),
    password: string({
      required_error: "password is required",
    }),
    fullname: string({ required_error: "fullname is required" }),
    role: z.enum(["admin", "master_admin"]),
  }),
});

export const updateAdminSchema = object({
  body: object({
    fullname: optional(string()),
    role: optional(z.enum(["admin", "master_admin"])),
    isActive: optional(boolean()),
  }),
  params: object({
    adminId: string({
      required_error: "AdminId is required",
    }),
  })
});

const params = {
  params: object({
    adminId: string({
      required_error: "AdminId is required",
    }),
  }),
};

export const deleteAdminSchema = object({ ...params });

export const adminLoginSchema = object({
  body: object({
    username: string({
      required_error: "username is required",
    }),
    password: string({
      required_error: "password is required",
    }),
  }),
});

export type CreateAdminInput = TypeOf<typeof createAdminSchema>;
export type AdminLoginSchema = TypeOf<typeof adminLoginSchema>;
export type DeleteAdminInput = TypeOf<typeof deleteAdminSchema>;
export type UpdateAdminInput = TypeOf<typeof updateAdminSchema>;
