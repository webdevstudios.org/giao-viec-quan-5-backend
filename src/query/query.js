db.vanbans.aggregate([
  {
    $match: {
      isDeleted: false,
      donViId: "626f52470f727512f6055d84",
    },
  },
  {
    $group: {
      _id: "$tinhTrangXuLyVanBanHanhChinh",
      total: { $sum: 1 },
    },
  },
]);

db.vanbans.countDocuments({
  isDeleted: false,
  tinhTrangXuLyVanBanDon: "Trễ",
  loaiVanBan: "Văn bản đơn",
});

db.vanbans.find({
  isDeleted: false,
  $or: [
    {
      thoiHan: { $lt: 1652684855770 + 86400, $gt: 1652684855770 },
    },
    {
      tinhTrangXuLyVanBanDon: "Trễ",
    },
  ],
});

db.vanbans.find({
  ngayBatDau: { $gt: 0 },
});
