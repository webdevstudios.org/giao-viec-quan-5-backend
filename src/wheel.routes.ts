import { Express } from "express";
import { createMazicHandler } from "./controller/mazic.controller";
import { createPresentHandler, getAllPresentsHandler } from "./controller/present.controller";
import { createSpinningHandler, getAllSpinningsHandler, spinTheWheelHandler } from "./controller/spinning.controller";
import { helloWheel } from "./controller/wheel.controller";
import validateResource from "./middleware/validateResource";
import { createMazicInput } from "./schema/mazic.schema";
import { spinTheWheelSchema } from "./schema/spinning.schema";


export default function wheelRoutes(app: Express) {
  const base_endpoint = "/api/wheel";

   /**
   * @openapi
   * '/api/wheel/present/create':
   *  post:
   *     tags:
   *     - Present
   *     summary: Create New Reward
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *           schema:
   *              $ref: '#/components/schemas/CreatePresentInput'
   *     responses:
   *      200:
   *        description: Success
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/CreatePresentResponse'
   *      409:
   *        description: Conflict
   *      400:
   *        description: Bad request
   */


  app.get(`${base_endpoint}/hello`, helloWheel);

  app.get(`${base_endpoint}/presents`, getAllPresentsHandler);

    /**
   * @openapi
   * /api/wheel/presents:
   *  get:
   *     tags:
   *     - Present
   *     summary: Get All Presents
   *     description: Get All Presents
   *     responses:
   *       200:
   *         description: App is up and running
   */


  app.post(`${base_endpoint}/present/create`, createPresentHandler);
  
  app.get(`${base_endpoint}/spinnings`, getAllSpinningsHandler);

      /**
   * @openapi
   * /api/wheel/spinnings:
   *  get:
   *     tags:
   *     - Spinning
   *     summary: Get All Spinning
   *     description: Get All Spinning
   *     responses:
   *       200:
   *         description: App is up and running
   */



  app.post(`${base_endpoint}/spinning/create` ,createSpinningHandler);

  app.post(`${base_endpoint}/spinTheWheel`,validateResource(spinTheWheelSchema), spinTheWheelHandler);

    /**
   * @openapi
   * '/api/wheel/spinTheWheel':
   *  post:
   *     tags:
   *     - Spinning
   *     summary: Get A Reward
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *           schema:
   *              $ref: '#/components/schemas/SpinTheWheelInput'
   *     responses:
   *      200:
   *        description: Success
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/SpinTheWheelResponse'
   *      409:
   *        description: Invalid Code
   *      400:
   *        description: Bad request
   */

  app.post(`${base_endpoint}/mazic/create`,validateResource(createMazicInput), createMazicHandler);


}
