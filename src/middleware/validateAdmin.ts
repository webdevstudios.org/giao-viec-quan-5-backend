import { Request, Response, NextFunction } from "express";
import { AdminDocument } from "../models/admin.model";

export const requireMasterAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = res.locals.user as AdminDocument;

  if (user?.role === "master_admin") {
    return next();
  }

  return res
    .status(403)
    .send("Do not have permission to perform this operation");
};

export const requireActiveAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = res.locals.user as AdminDocument;

  if (user?.isActive) {
    return next();
  }

  return res
    .status(403)
    .send("Do not have permission to perform this operation");
};


export const requireAdminUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = res.locals.user as AdminDocument;

  if (user) {
    return next();
  }

  return res
    .status(403)
    .send("Do not have permission to perform this operation");
};

