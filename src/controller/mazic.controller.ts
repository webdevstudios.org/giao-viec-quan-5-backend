import { Request, Response } from "express";
import { CreateMazicInput } from "../schema/mazic.schema";
import { CreateSpinningInput } from "../schema/spinning.schema";
import { createMazic } from "../service/mazic.service";
import { findPresent } from "../service/present.service";
import { createSpinning, findSpinning } from "../service/spinning.service";
import appLogger from "../utils/logger";

export async function createMazicHandler(
  req: Request<{}, {}, CreateMazicInput["body"]>,
  res: Response
) {
  try {
    const body = req.body;
    const spinning = await findSpinning(req.body.spinningCode);
    const present = await findPresent(req.body.presentId);
    if (spinning && present) {
      const mazic = await createMazic({
        spinningCode: spinning.code,
        presentId: present._id,
      });
      return res.send(mazic);
    }
    return res.status(409).send("invalid");
  } catch (error) {
    appLogger.info(error);
    return res.status(409);
  }
}
