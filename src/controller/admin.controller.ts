import { Request, Response } from "express";
import {
  CreateAdminInput,
  DeleteAdminInput,
  UpdateAdminInput,
} from "../schema/admin.schema";
import {
  createAdmin,
  deleteAdmin,
  findAdminById,
  findAndUpdateAdmin,
  getAllAdmins,
} from "../service/admin.service";
import appLogger from "../utils/logger";

export async function createAdminHandler(
  req: Request<{}, {}, CreateAdminInput["body"]>,
  res: Response
) {
  try {
    const admin = await createAdmin(req.body);
    return res.send(admin);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}

export async function getAllAdminsHandler(req: Request, res: Response) {
  try {
    const admins = await getAllAdmins({ isDeleted: false });
    return res.send(admins);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}

export async function deleteAdminHandler(
  req: Request<DeleteAdminInput["params"]>,
  res: Response
) {
  const userId = res.locals.user._id;
  const adminIdToDelete = req.params.adminId;
  if (userId === adminIdToDelete)
    return res.status(409).send("Can not delete yourself");

  await deleteAdmin(adminIdToDelete);

  return res.status(200).send("successful");
}
export async function updateAdminHandler(
  req: Request<UpdateAdminInput["params"], {}, UpdateAdminInput["body"]>,
  res: Response
) {
  const userId = res.locals.user._id;
  const adminIdToDelete = req.params.adminId;
  if (userId === adminIdToDelete && req.body.isActive === false)
    return res.status(400).send("Không thể khóa kích hoạt tài khoản này");

  const admin = await findAdminById(req.params.adminId);
  if (!admin) return res.status(404).send("Tài khoản admin không tồn tại");

  const upadatedAdmin = await findAndUpdateAdmin(
    { _id: req.params.adminId },
    req.body,
    { new: true }
  );

  return res.send(upadatedAdmin);
}
