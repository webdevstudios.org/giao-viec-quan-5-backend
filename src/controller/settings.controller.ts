import { NextFunction, Request, Response } from "express";
import {
  Quan5Enums,
  LoaiDon,
  LoaiVanBan,
  TinhTrangXuLyVanBanDon,
  TinhTrangXuLyVanBanHanhChinh,
} from "../shared/constants";
import StatusCodes from "http-status-codes";

export async function getAllEnumsHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    res.send({
      Quan5Enums,
      LoaiDon,
      LoaiVanBan,
      TinhTrangXuLyVanBanDon,
      TinhTrangXuLyVanBanHanhChinh,
    });
  } catch (e) {
    res.status(StatusCodes.FORBIDDEN).send("Unauthorize");
  }
}
