import { Request, Response } from "express";
import fs from "fs";
import mongoose, { FilterQuery, Types } from "mongoose";
import { VanBanDocument } from "../models/vanban.model";
import { getExcelStatisticFile } from "../service/excel.service/excel.service";
import {
  CountAndGroupVanBanByStatus,
  countVanBan,
  getVanBans,
} from "../service/vanban.service";
import {
  countdownDeadline,
  TinhTrangXuLyVanBanDon,
  TinhTrangXuLyVanBanHanhChinh,
} from "../shared/constants";
import appLogger from "../utils/logger";

export interface GroupByStatusVanBanDonSchema {
  DangGiaiQuyet: number;
  DaGiaiQuyet: number;
  Tre: number;
}

export interface GroupByStatusVanBanHanhChinhSchema {
  ChuanBiTrienKhai: number;
  DangThucHien: number;
  DaHoanThanh: number;
  DaHuy: number;
}

export interface StatisticSchema {
  loaiVanBan: string;
  donViId: string;
  tuNgay: number;
  denNgay: number;
}

export interface StatisticResponseSchema {
  vanBanDon: {
    daGiaiQuyet: number;
    dangGiaiQuyet: number;
    tre: number;
  };
  hanhChinh: {
    daHoanThanh: number;
    chuanBiTrienKhai: number;
    dangThucHien: number;
    daHuy: number;
  };
  working: number;
  completed: number;
  late: number;
  total: number;
  cancel: number;
  van_ban_can_hoan_thanh: any[];
  van_ban_qua_han: any[];
}

const ObjectId = mongoose.Types.ObjectId;

export async function getOverviewStatisticHandler(
  req: Request<{}, {}, {}, StatisticSchema>,
  res: Response<any>
) {
  const { tuNgay, denNgay, donViId } = req.query;

  console.log("get over ");

  let queryParam: FilterQuery<VanBanDocument> = { isDeleted: false };

  let extraQuery = { ...queryParam };

  if (donViId) {
    let id = new mongoose.Types.ObjectId(donViId);
    extraQuery = {
      ...extraQuery,
      donViId: id,
    };
  }

  if (tuNgay && denNgay) {
    extraQuery = {
      ...extraQuery,
      ngayBatDau: { $lte: denNgay * 1, $gte: tuNgay * 1 },
    };
  }

  const statisticVanBanByStatus = await CountAndGroupVanBanByStatus({
    ...extraQuery,
  });

  console.log("by status", statisticVanBanByStatus);

  const currentTimestamp = new Date(Date.now()).getTime();

  const vanBanGanDenHan = await getVanBans({
    ...extraQuery,
    $or: [
      {
        $and: [
          {
            thoiHan: {
              $lte: currentTimestamp + countdownDeadline,
              $gte: currentTimestamp,
            },
          },
          {
            $and: [
              {
                tinhTrangXuLyVanBanDon: {
                  $nin: [
                    TinhTrangXuLyVanBanDon.DaGiaiQuyet,
                    TinhTrangXuLyVanBanDon.DaHuy,
                  ],
                },
              },
              {
                tinhTrangXuLyVanBanHanhChinh: {
                  $nin: [
                    TinhTrangXuLyVanBanHanhChinh.DaHoanThanh,
                    TinhTrangXuLyVanBanHanhChinh.DaHuy,
                  ],
                },
              },
            ],
          },
        ],
      },
      {
        tinhTrangXuLyVanBanDon: TinhTrangXuLyVanBanDon.Tre,
      },
    ],
  });

  const vanBanQuaHan = await getVanBans({
    ...extraQuery,
    thoiHan: { $lt: currentTimestamp },
    tinhTrangXuLyVanBanDon: {
      $nin: [
        TinhTrangXuLyVanBanDon.DaGiaiQuyet,
        TinhTrangXuLyVanBanDon.Tre,
        TinhTrangXuLyVanBanDon.DaHuy,
      ],
    },
    tinhTrangXuLyVanBanHanhChinh: {
      $nin: [
        TinhTrangXuLyVanBanHanhChinh.DaHoanThanh,
        TinhTrangXuLyVanBanHanhChinh.DaHuy,
      ],
    },
  });
  const vanBanDaHuy = await getVanBans({
    ...extraQuery,
    tinhTrangXuLyVanBanDon: {
      $nin: [
        TinhTrangXuLyVanBanDon.DaGiaiQuyet,
        TinhTrangXuLyVanBanDon.DangGiaiQuyet,
        TinhTrangXuLyVanBanDon.Tre,
      ],
    },
    tinhTrangXuLyVanBanHanhChinh: {
      $nin: [
        TinhTrangXuLyVanBanHanhChinh.DaHoanThanh,
        TinhTrangXuLyVanBanHanhChinh.DangThucHien,
        TinhTrangXuLyVanBanHanhChinh.ChuanBiTrienKhai,
      ],
    },
  });

  const result: StatisticResponseSchema = {
    vanBanDon: statisticVanBanByStatus.vanBanDon,
    hanhChinh: statisticVanBanByStatus.hanhChinh,
    working:
      statisticVanBanByStatus?.vanBanDon?.dangGiaiQuyet +
      +statisticVanBanByStatus?.hanhChinh?.chuanBiTrienKhai +
      statisticVanBanByStatus?.hanhChinh?.dangThucHien,
    completed:
      statisticVanBanByStatus?.vanBanDon?.daGiaiQuyet +
      statisticVanBanByStatus?.hanhChinh?.daHoanThanh,
    late: vanBanQuaHan.length,
    cancel:vanBanDaHuy.length,
    total: await countVanBan({ ...extraQuery }),
    van_ban_can_hoan_thanh: vanBanGanDenHan,
    van_ban_qua_han: vanBanQuaHan,
  };

  return res.send({
    ...result,
  });
}

export async function exportExcelHandler(
  req: Request<{}, {}, {}, StatisticSchema>,
  res: Response
) {
  try {
    const { tuNgay, denNgay, donViId } = req.query;

    let queryParam: FilterQuery<VanBanDocument> = { isDeleted: false };

    let extraQuery = { ...queryParam };

    if (donViId) extraQuery = { ...extraQuery, donViId };

    if (tuNgay && denNgay) {
      extraQuery = {
        ...extraQuery,
        ngayBatDau: { $lte: denNgay * 1, $gte: tuNgay * 1 },
      };
    }

    const fileName = await getExcelStatisticFile(extraQuery);
    res.setHeader("Content-Disposition", `attachment; filename=report.xlsx`);
    res.status(200).sendFile(fileName, (err) => {
      if (err) {
        console.log(err);
      } else {
        fs.unlink(fileName, (error) => {
          if (error) console.log(error);
        });
      }
    });
  } catch (error) {
    throw error;
  }
}
