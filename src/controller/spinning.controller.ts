import { Request, Response } from "express";
import {
  CreateSpinningInput,
  SpinTheWheelInput,
} from "../schema/spinning.schema";
import { findCorrespondencePresent } from "../service/mazic.service";
import {
  findPresent,
  randomPresent,
  updatePresent,
} from "../service/present.service";
import {
  createSpinning,
  findAndUpdateSpinning,
  getAllSpinnings,
  isSpinningAvailable,
} from "../service/spinning.service";
import appLogger from "../utils/logger";

export async function createSpinningHandler(
  req: Request<{}, {}, CreateSpinningInput>,
  res: Response
) {
  try {
    const body = req.body;
    const spinning = await createSpinning({ ...body });

    return res.send(spinning);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}

export async function updateSpinningHandler(
  body: SpinTheWheelInput["body"],
  presentId: string
) {
  await findAndUpdateSpinning(
    {
      code: body.code,
    },
    {
      $set: {
        brand: body.brand,
        clientFullname: body.clientFullname,
        presentId: presentId,
        available: false,
      },
    },
    {}
  );
}

export async function spinTheWheelHandler(
  req: Request<{}, {}, SpinTheWheelInput["body"]>,
  res: Response
) {
  try {
    const spinning = await isSpinningAvailable(req.body.code);
    if (!spinning) {
      return res.status(409).send("Invalid code");
    }

    const fixPresentId = await findCorrespondencePresent(req.body.code);

    if (fixPresentId) {
      const fixPresent = await findPresent(fixPresentId);
      updateSpinningHandler(req.body,fixPresent?._id);
      console.log("fix",fixPresentId);
      return res.send(fixPresent);
    }

    const present = await randomPresent();

    await updatePresent({ _id: present._id }, { $inc: { number: -1 } }, {});

    await findAndUpdateSpinning(
      {
        code: req.body.code,
      },
      {
        $set: {
          brand: req.body.brand,
          clientFullname: req.body.clientFullname,
          presentId: present._id,
          available: false,
        },
      },
      {}
    );
    return res.send(present);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}

export async function getAllSpinningsHandler(req: Request, res: Response) {
  try {
    const spinnings = await getAllSpinnings();
    return res.send(spinnings);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}
