import { Request, Response } from "express";
import { AdminLoginSchema } from "../schema/admin.schema";
import { validateAdminPassword } from "../service/admin.service";
import { signJwt } from "../utils/jwt.utils";
import config from "config";
import { oneDayTimestamp } from "../shared/constants";

export async function adminLoginHandler(
  req: Request<{}, {}, AdminLoginSchema["body"]>,
  res: Response
) {
  const admin = await validateAdminPassword(req.body);

  console.log(admin);

  if (!admin) return res.status(401).send("Invalid username or password");

  if (admin.isActive === false)
    return res.status(401).send("Tài khoản đã bị khóa");

  const accessToken = signJwt({ ...admin }, "accessTokenPrivateKey", {
    expiresIn: "10 days",
  });
  return res.send({
    accessToken,
    expiresIn: new Date(Date.now()).getTime() + oneDayTimestamp * 10,
    user: admin,
  });
}
