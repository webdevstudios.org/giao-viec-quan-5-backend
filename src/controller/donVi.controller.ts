import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { DonViAttributes } from "../models/donVi.model";
import {
  createDonVi,
  deleteDonViById,
  findDonViById,
  getAllDonVi,
  updateDonVi,
} from "../service/donVi.service";
import { findVanBanById, getVanBans } from "../service/vanban.service";
import appLogger from "../utils/logger";
import { createDonViSchema } from "../validator/donVi.validator";

export async function helloDonVi(req: Request, res: Response) {
  try {
    res.send("Hello");
  } catch (error) {
    appLogger.info(error);
    return res.status(409);
  }
}

export async function createDonViHandler(
  req: Request<{}, {}, DonViAttributes>,
  res: Response
) {
  try {
    const value = await createDonViSchema.validateAsync(req.body);
    const donVi = await createDonVi(value);
    res.send(donVi);
  } catch (e: any) {
    return res.status(409).send(e.message);
  }
}
export async function updateDonViHandler(
  req: Request<{ id: string }, {}, DonViAttributes>,
  res: Response
) {
  try {
    const value = await createDonViSchema.validateAsync(req.body);
    const updatedDonVi = await updateDonVi({ _id: req.params.id }, value, {
      new: true,
    });
    res.send(updatedDonVi);
  } catch (e: any) {
    res.status(409).send(e.message);
  }
}

export async function getAllDonViHandler(req: Request, res: Response) {
  const donVis = await getAllDonVi();
  return res.send(donVis);
}

export async function deleteDonViHandler(
  req: Request<{ id: string }>,
  res: Response
) {
  try {
    const vanbans = await getVanBans({
      donViId: req.params.id,
      isDeleted: false,
    });
    if (vanbans.length > 0) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .send("Không thể xóa do có văn bản thuộc đơn vị này");
    }
    const donVi = await findDonViById(req.params.id);
    if (!donVi)
      return res.status(StatusCodes.NOT_FOUND).send("Không tìm thấy đơn vị");
    await deleteDonViById(req.params.id);
    return res.status(200).send("successfully");
  } catch (e: any) {
    res.status(500).send(e.message);
  }
}
