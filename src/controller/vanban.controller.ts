import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { omit } from "lodash";
import { AdminDocument } from "../models/admin.model";
import {
  FilterVanBanSchema,
  VanBanAttributes,
  VanBanDocument,
} from "../models/vanban.model";
import { findAdminById } from "../service/admin.service";
import { findDonViById } from "../service/donVi.service";
import {
  createVanBan,
  deteleVanBanById,
  findVanBanById,
  getAmountVanBan,
  getVanBans,
  updateVanBan,
} from "../service/vanban.service";
import { iResponsePayload } from "../shared/iResponsePayload";
import {
  createVanBanSchema,
  getVanBanQuerySchema,
  updateVanBanSchema,
} from "../validator/vanban.validator";

export async function createVanBanHandler(
  req: Request<{}, {}, VanBanAttributes>,
  res: Response
) {
  try {
    //check exist donVi
    //validate input
    let value;
    try {
      value = await createVanBanSchema.validateAsync(req.body);
    } catch (error: any) {
      return res.status(StatusCodes.BAD_REQUEST).send(error.message);
    }

    const donVi = await findDonViById(value.donViId);
    if (!donVi) {
      return res.status(StatusCodes.NOT_FOUND).send("Không tìm thấy đơn vị");
    }

    const adminId = res.locals.user._id;
    let ngayBatDau = value.ngayBatDau ? value.ngayBatDau : value.ngayBanHanh;
    const vanBanToCreate: VanBanAttributes = {
      ...value,
      ngayBatDau,
      adminId: adminId,
    };
    const newVanBan = await createVanBan(vanBanToCreate);
    return res.send({ vanBan: newVanBan, donVi: donVi });
  } catch (error: any) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
  }
}

export async function updateVanBanHandler(
  req: Request<{ vanBanId: string }, {}, VanBanAttributes>,
  res: Response
) {
  try {
    //validate input
    let update;
    try {
      update = await updateVanBanSchema.validateAsync(req.body);
    } catch (error: any) {
      return res.status(StatusCodes.BAD_REQUEST).send(error.message);
    }

    //check exist van ban
    const vanBanId = req.params.vanBanId;
    const vanBan = await findVanBanById(vanBanId);
    if (!vanBan) {
      return res.status(StatusCodes.NOT_FOUND).send("Không tìm thấy văn bản");
    }

    //check exit don vi
    if (update.donViId) {
      const donVi = await findDonViById(update.donViId);
      if (!donVi) {
        return res.status(StatusCodes.NOT_FOUND).send("Không tìm thấy đơn vị");
      }
    }

    // //check is Owner editting docs
    // const admin = res.locals.user as AdminDocument;
    // if (admin._id !== vanBan?.adminId && admin.role !== "master_admin") {
    //   return res
    //     .status(StatusCodes.FORBIDDEN)
    //     .send("Chỉ người tạo văn bản mới có quyền chỉnh sửa");
    // }

    let ngayBatDau = update.ngayBatDau ? update.ngayBatDau : update.ngayBanHanh;
    update = { ...update, ngayBatDau };

    const updatedVanBan = await updateVanBan(vanBanId, update, {
      new: true,
    });
    return res.send(updatedVanBan);
  } catch (error: any) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
  }
}

export async function getVanBansHandler(
  req: Request<{}, {}, {}, FilterVanBanSchema>,
  res: Response
) {
  try {
    await getVanBanQuerySchema.validateAsync(req.query);
    const query = req.query;
    const { pageNo, size, search } = query;

    if (pageNo < 0 || pageNo === 0) {
      const response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.send(response);
    }

    let skip = size * (pageNo - 1);
    let limit = size * 1;

    let new_query;

    if (search) {
      new_query = { ...query, $text: { $search: search } };
    } else {
      new_query = { ...query };
    }

    new_query = { ...new_query, isDeleted: false };

    const vanbans = await getVanBans(new_query, { skip, limit, lean: true });
    let numberVanBan = await getAmountVanBan({ ...new_query, skip, limit });

    const payload: iResponsePayload<any> = {
      data: vanbans,
      message: "successful",
      pageNo: pageNo,
      size: size,
      total: numberVanBan,
    };
    return res.send(payload);
  } catch (error: any) {
    console.log(error);
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error.message);
  }
}

export async function deleteVanBanHandler(
  req: Request<{ vanBanId: string }>,
  res: Response
) {
  try {
    const vanBanId = req.params.vanBanId;
    const result = await deteleVanBanById(vanBanId);
    return res.status(StatusCodes.OK).send(result);
  } catch (e: any) {
    console.log("error controller", e);
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e.message);
  }
}
export async function getVanBanById(
  req: Request<{ vanBanId: string }>,
  res: Response
) {
  try {
    const vanBanId = req.params.vanBanId;
    const result = await findVanBanById(vanBanId);
    if (!result)
      return res.status(StatusCodes.NOT_FOUND).send("Không tìm thấy văn bản");

    //find admin creating docs
    const adminId = result.adminId;

    const admin = await findAdminById(adminId);

    return res.status(StatusCodes.OK).send({
      ...result.toObject(),
      adminInfo: omit(admin?.toJSON(), "password"),
    });
  } catch (e: any) {
    console.log("error controller", e);
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e.message);
  }
}
