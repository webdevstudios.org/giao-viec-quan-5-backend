import { Request, Response } from "express";
import { CreatePresentInput } from "../schema/present.shema";
import { createPresent, getAllPresents } from "../service/present.service";
import { createProduct } from "../service/product.service";
import appLogger from "../utils/logger";

export async function createPresentHandler(
  req: Request<{}, {}, CreatePresentInput["body"]>,
  res: Response
) {
  try {
    const body = req.body;
    const present = await createPresent(body);

    return res.send(present);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}

export async function getAllPresentsHandler(req: Request, res: Response) {
  try {
    const presents = await getAllPresents();
    return res.send(presents);
  } catch (e: any) {
    appLogger.error(e);
    return res.status(409).send(e.message);
  }
}
