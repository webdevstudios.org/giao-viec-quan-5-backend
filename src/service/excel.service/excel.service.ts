import exceljs, { AddWorksheetOptions } from "exceljs";
import { FilterQuery } from "mongoose";
import { VanBanDocument } from "../../models/vanban.model";
import { LoaiVanBan } from "../../shared/constants";
import { createSheet, writeExcelToDisk } from "../../shared/excel";
import {
  arrayStringToParagraph,
  convertTimestampToDate,
} from "../../utils/helpers";
import { findAdminById } from "../admin.service";
import { findDonViById } from "../donVi.service";
import { getVanBans } from "../vanban.service";
import { vanBanDonColumns, vanBanHanhChinhColumns } from "./excel.columns";

export async function getExcelStatisticFile(
  query: FilterQuery<VanBanDocument>
) {
  const workbook = new exceljs.Workbook();
  const vanbanHanhChinh = await getVanBans({
    ...query,
    loaiVanBan: LoaiVanBan.HanhChinh,
  });
  const exportVanBansHanhChinh = await Promise.all(
    vanbanHanhChinh.map(async (vanban, index) => {
      let donViName = "";
      let adminName = "";

      const [ngayBatDau, thoiHan, ngayBanHanh] = convertTimestampToDate(
        vanban.ngayBatDau,
        vanban.thoiHan,
        vanban.ngayBanHanh
      );

      // format array of string
      let noiDungChiDao = "";
      let noiDungVanBanHanhChinh = "";
      try {
        noiDungChiDao = arrayStringToParagraph(
          JSON.parse(vanban.noiDungChiDao)
        );
        noiDungVanBanHanhChinh = arrayStringToParagraph(
          JSON.parse(vanban.noiDungVanBanHanhChinh)
        );
      } catch (error) {}

      if (vanban.donViId) {
        const donVi = await findDonViById(vanban.donViId);
        donViName = donVi?.name + "";
      }
      if (vanban.adminId) {
        const adminInfo = await findAdminById(vanban.adminId);
        adminName = adminInfo?.fullname + "";
      }
      const _id = vanban._id.toString();
      const result = {
        ...vanban,
        index: index + 1,
        adminName,
        donViName,
        _id,
        ngayBatDau,
        thoiHan,
        noiDungChiDao,
        noiDungVanBanHanhChinh,
        ngayBanHanh,
      };
      return result;
    })
  );
  const vanbanDon = await getVanBans({
    ...query,
    loaiVanBan: LoaiVanBan.VanBanDon,
  });
  const exportVanBansDon = await Promise.all(
    vanbanDon.map(async (vanban, index) => {
      let donViName = "";
      let adminName = "";

      console.log(vanban);

      const [ngayBatDau, thoiHan, ngayBanHanh, ngayNhanDon] =
        convertTimestampToDate(
          vanban.ngayBatDau,
          vanban.thoiHan,
          vanban.ngayBanHanh,
          vanban.ngayNhanDon
        );

      // format array of string
      let noiDungDon = "";
      let noiDungVanBanPhanHoi = "";
      try {
        console.log(vanban.noiDungDon);
        noiDungDon = arrayStringToParagraph(JSON.parse(vanban.noiDungDon));
        noiDungVanBanPhanHoi = arrayStringToParagraph(
          JSON.parse(vanban.noiDungVanBanPhanHoi)
        );
      } catch (error) {}

      // -----------------------------------

      if (vanban.donViId) {
        const donVi = await findDonViById(vanban.donViId);
        donViName = donVi?.name + "";
      }
      if (vanban.adminId) {
        const adminInfo = await findAdminById(vanban.adminId);
        adminName = adminInfo?.fullname + "";
      }
      const _id = vanban._id.toString();
      const result = {
        ...vanban,
        index: index + 1,
        adminName,
        donViName,
        _id,
        ngayBatDau,
        thoiHan,
        ngayBanHanh,
        noiDungDon,
        ngayNhanDon,
        noiDungVanBanPhanHoi,
      };
      return result;
    })
  );

  const options: Partial<AddWorksheetOptions> = {
    pageSetup: {
      orientation: "landscape",
      paperSize:9
    },
    
  };

  await createSheet(
    exportVanBansHanhChinh,
    vanBanHanhChinhColumns,
    "Văn bản hành chính",
    workbook,
    "A1:M1",
    options
  );
  await createSheet(
    exportVanBansDon,
    vanBanDonColumns,
    "Văn bản đơn",
    workbook,
    "A1:O1",
    options
  );

  const fileName = await writeExcelToDisk(workbook);
  return fileName;
}
