export const vanBanHanhChinhColumns = [
  { header: "STT", key: "index", width: 6 },
  { header: "Số ký hiệu văn bản", key: "soKyHieu", width: 10 },
  { header: "Ngày ban hành/ chỉ đạo", key: "ngayBanHanh", width: 15 },
  { header: "Tên VB/ Trích yếu văn bản", key: "tenVanBan", width: 15 },
  { header: "Nội dung chỉ đạo", key: "noiDungChiDao", width: 15 },
  { header: "Đơn vị thực hiện", key: "donViName", width: 15 },
  { header: "Ngày bắt đầu", key: "ngayBatDau", width: 15 },
  { header: "Thời hạn", key: "thoiHan", width: 15 },
  { header: "Chuyên viên theo dõi xử lý", key: "chuyenVienTheoDoi", width: 15 },
  {
    header: "Tình Trạng Xử Lý",
    key: "tinhTrangXuLyVanBanHanhChinh",
    width: 15,
  },
  { header: "Nội dung văn bản/ Tiến độ thực hiện", key: "noiDungVanBanHanhChinh", width: 15 },
  { header: "Cơ quan ban hành", key: "coQuanBanHanh", width: 15 },


  { header: "Ghi chú/ Kết quả thực hiện", key: "ghiChu", width: 15 },
];
export const vanBanDonColumns = [
  { header: "STT", key: "index", width: 6 },
  { header: "Loại đơn", key: "loaiDon", width: 15 },
  { header: "Tên đơn", key: "tenVanBan", width: 15 },
  { header: "Nội dung đơn", key: "noiDungDon", width: 15 },
  { header: "Họ Tên Người Gửi", key: "hoTenNguoiGui", width: 15 },
  { header: "Chuyển đơn vị xử lý", key: "donViName", width: 15 },
  { header: "Ngày chuyển đơn", key: "ngayBatDau", width: 15 },
  { header: "Thời hạn", key: "thoiHan", width: 15 },
  { header: "Tình Trạng Xử Lý", key: "tinhTrangXuLyVanBanDon", width: 15 },
  
  { header: "Ngày nhận đơn", key: "ngayNhanDon", width: 15 },
  { header: "Ghi chú", key: "ghiChu", width: 15 },
  { header: "Hướng xử lý", key: "huongXuLy", width: 15 },
  {
    header: "Nội dung văn bản phản hồi",
    key: "noiDungVanBanPhanHoi",
    width: 15,
  },
  { header: "Số điện thoại người gửi", key: "sdtNguoiGui", width: 15 },
  { header: "CCCD Người gửi", key: "cccdNguoiGui", width: 15 },
];
