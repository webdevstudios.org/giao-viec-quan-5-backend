import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import PresentModel, {
  PresentDocument,
  PresentInput,
} from "../models/present.model";
import SpinningModel from "../models/spinning.model";
import appLogger from "../utils/logger";

export async function createPresent(input: PresentInput) {
  try {
    const result = await PresentModel.create(input);
    return result;
  } catch (e) {
    throw e;
  }
}

export async function getAllPresents() {
  try {
    const result = await PresentModel.find({}).lean();
    return result;
  } catch (e) {
    throw e;
  }
}

export async function findPresent(id: string) {
  console.log(id);
  const result =  await PresentModel.findOne({ _id: id });
  return result;
}

export async function randomPresent() {
  try {
    const query: FilterQuery<PresentDocument> = {
      status: true,
      number: { $gt: 0 },
    };

    const available_presents = await PresentModel.find(query).lean();

 
    var present =
      available_presents[Math.floor(Math.random() * available_presents.length)];

    return present;
  } catch (e) {
    throw e;
  }
}

export async function updatePresent(
  query: FilterQuery<PresentDocument>,
  update: UpdateQuery<PresentDocument>,
  options: QueryOptions
) {
  return await PresentModel.findOneAndUpdate(query, update, options);
}
