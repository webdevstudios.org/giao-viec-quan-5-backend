import { omit } from "lodash";
import { FilterQuery, QueryOptions, UpdateQuery, _FilterQuery } from "mongoose";
import AdminModel, { AdminDocument, AdminInput } from "../models/admin.model";
import appLogger from "../utils/logger";

export async function createAdmin(input: AdminInput) {
  try {
    const admin = await AdminModel.create(input);
    return omit(admin.toJSON(), "password");
  } catch (e: any) {
    throw new Error(e);
  }
}

export async function validateAdminPassword({
  username,
  password,
}: {
  username: string;
  password: string;
}) {
  const admin = await AdminModel.findOne({ username });
  if (!admin) {
    return false;
  }

  const isValid = await admin.comparePassword(password);

  if (!isValid) return false;

  return omit(admin.toJSON(), "password");
}

export async function getAllAdmins(query:FilterQuery<AdminDocument>) {
  return await AdminModel.find(query).lean();
}

export async function createFirstAdmin() {
  let new_admin: AdminInput = {
    fullname: process.env.MASTER_ADMIN_FULLNAME as string,
    username: process.env.MASTER_ADMIN_USERNAME as string,
    password: process.env.MASTER_ADMIN_PASSWORD as string,
    role: "master_admin",
  };
  try {
    await AdminModel.create(new_admin);
    console.log(new_admin);
  } catch (e: any) {
    appLogger.error(e);
  }

  return;
}

export async function deleteAdmin(id: string) {
  return await AdminModel.updateOne({ _id: id }, { isDeleted: true });
}

export async function findAndUpdateAdmin(
  query: FilterQuery<AdminDocument>,
  update: UpdateQuery<AdminDocument>,
  options: QueryOptions
) {
  return AdminModel.findOneAndUpdate(query, update, options);
}

export async function findAdminById(id: string) {
  return await AdminModel.findById(id);
}
