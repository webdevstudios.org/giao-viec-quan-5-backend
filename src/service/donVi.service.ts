import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import DonViModel, {
  DonViAttributes,
  DonViDocument,
} from "../models/donVi.model";

export async function createDonVi(input: DonViAttributes) {
  try {
    const result = await DonViModel.create(input);
    return result;
  } catch (e) {
    throw e;
  }
}

export async function findDonViById(id: string) {
  try {
    return await DonViModel.findById(id, {}, { lean: true });
  } catch (e) {
    return null;
  }
}

export async function getAllDonVi() {
  try {
    return await DonViModel.find({}).lean();
  } catch (e) {
    throw e;
  }
}

export async function updateDonVi(
  query: FilterQuery<DonViDocument>,
  update: UpdateQuery<DonViDocument>,
  options: QueryOptions
) {
  try {
    return await DonViModel.findOneAndUpdate(query, update, options);
  } catch (e) {
    throw e;
  }
}

export async function deleteDonViById(id: string) {
  return await DonViModel.findByIdAndUpdate(id, { isDeleted: true });
}
