import { FilterQuery, QueryOptions, UpdateQuery, _FilterQuery } from "mongoose";
import SpinningModel, {
  SpinningDocument,
    SpinningInput
} from "../models/spinning.model";
import appLogger from "../utils/logger";

export async function createSpinning(input: SpinningInput) {
  try {
    const result = await SpinningModel.create(input);
    return result;
  } catch (e) {
    throw e;
  }
}

export async function findSpinning(code:string) {
    try {
        const result = await SpinningModel.findOne({code},{},{lean:true});
        return result;
    } catch (e) {
        throw e;
    }
}
export async function isSpinningAvailable(code:string) {
    try {
        appLogger.info(code);
        const result = await SpinningModel.findOne({code,available:true},{},{lean:true});
        return result;
    } catch (e) {
        throw e;
    }
}

export async function findAndUpdateSpinning(
  query:FilterQuery<SpinningDocument>,
  update: UpdateQuery<SpinningDocument>,
  options: QueryOptions
) {
  return SpinningModel.findOneAndUpdate(query, update, options);
}



export async function getAllSpinnings() {
  try {
    const result = await SpinningModel.find({}).lean();
    return result;
  } catch (e) {
    throw e;
  }
}
