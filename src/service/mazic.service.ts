import MazicModel, { MazicInput } from "../models/mazic.model";
import { getAllPresents } from "./present.service";

export async function createMazic(input: MazicInput) {
  const result = await MazicModel.create(input);
  return result;
}

export async function findCorrespondencePresent(code: string) {
  const result = await MazicModel.findOne({ spinningCode: code });
  if (result) {
    return result.presentId;
  } else return null;
}

export async function getAllMazic() {
  try {
    const result = await MazicModel.find({}).lean();
    return result;
  } catch (e) {
    throw e;
  }
}
