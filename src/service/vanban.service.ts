import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import {
  GroupByStatusVanBanDonSchema,
  StatisticResponseSchema,
} from "../controller/statistic.controller";
import VanBanModel, {
  VanBanAttributes,
  VanBanDocument,
} from "../models/vanban.model";
import {
  LoaiVanBan,
  TinhTrangXuLyVanBanDon,
  TinhTrangXuLyVanBanHanhChinh,
} from "../shared/constants";
import { getValueOfObjectArray } from "../utils/helpers";

export async function createVanBan(input: VanBanAttributes) {
  try {
    const result = await VanBanModel.create(input);
    return result;
  } catch (e) {
    throw e;
  }
}

export async function updateVanBan(
  id: string,
  update: UpdateQuery<VanBanDocument>,
  options: QueryOptions = { lean: true }
) {
  try {
    const result = await VanBanModel.findByIdAndUpdate(id, update, options);
    console.log("update van ban", result);
    return result;
  } catch (e) {
    throw e;
  }
}

export async function getVanBans(
  query: FilterQuery<VanBanDocument>,
  options: QueryOptions = { lean: true }
) {
  try {
    console.log("gets van ban service, query", query);
    const result = await VanBanModel.find(query, {}, options).lean();
    return result;
  } catch (e) {
    return [];
  }
}

export async function getAmountVanBan(query: FilterQuery<VanBanDocument>) {
  try {
    const number = await VanBanModel.count(query);
    return number;
  } catch (e) {
    return 0;
  }
}

export async function findVanBanById(id: string) {
  try {
    const result = await VanBanModel.findOne({ _id: id });
    return result;
  } catch (e) {
    return null;
  }
}

export async function deteleVanBanById(id: string) {
  try {
    const result = await VanBanModel.findByIdAndUpdate(id, { isDeleted: true });
    return result;
  } catch (e) {
    console.log("err", e);
    throw e;
  }
}

export async function countVanBan(query: FilterQuery<VanBanDocument>) {
  try {
    console.log("query Count Van Ban", query);
    const result = await VanBanModel.countDocuments({ ...query });
    return result;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export async function countGroupBy(
  query: FilterQuery<VanBanDocument>,
  group_by: string
): Promise<{ _id: string; total: number }[]> {
  try {
    const result = await VanBanModel.aggregate([
      { $match: { ...query } },
      {
        $group: {
          _id: group_by,
          total: { $sum: 1 },
        },
      },
    ]);
    return result;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export async function CountAndGroupVanBanByStatus(
  query: FilterQuery<VanBanDocument>
) {
  const vanBanHanhChinh = await countGroupBy(
    query,
    "$tinhTrangXuLyVanBanHanhChinh"
  );
  console.log("querrry", query);
  const vanBanDon = await countGroupBy(query, "$tinhTrangXuLyVanBanDon");

  console.log(vanBanDon, vanBanHanhChinh);

  const { DaGiaiQuyet, DangGiaiQuyet, Tre } = TinhTrangXuLyVanBanDon;
  const { ChuanBiTrienKhai, DaHoanThanh, DaHuy, DangThucHien } =
    TinhTrangXuLyVanBanHanhChinh;

  const vanBanDon_tre = getValueOfObjectArray(vanBanDon, Tre, "_id", "total");
  const vanBanDon_dangThucHien = getValueOfObjectArray(
    vanBanDon,
    DangGiaiQuyet,
    "_id",
    "total"
  );
  const vanBanDon_hoanThanh = getValueOfObjectArray(
    vanBanDon,
    DaGiaiQuyet,
    "_id",
    "total"
  );

  const hanhChinh_dangThucHien = getValueOfObjectArray(
    vanBanHanhChinh,
    DangThucHien,
    "_id",
    "total"
  );
  const hanhChinh_chuanBiTrienKhai = getValueOfObjectArray(
    vanBanHanhChinh,
    ChuanBiTrienKhai,
    "_id",
    "total"
  );
  const hanhChinh_hoanThanh = getValueOfObjectArray(
    vanBanHanhChinh,
    DaHoanThanh,
    "_id",
    "total"
  );
  const hanhChinh_daHuy = getValueOfObjectArray(
    vanBanHanhChinh,
    DaHuy,
    "_id",
    "total"
  );

  const result: Pick<StatisticResponseSchema, "vanBanDon" | "hanhChinh"> = {
    vanBanDon: {
      daGiaiQuyet: vanBanDon_hoanThanh,
      dangGiaiQuyet: vanBanDon_dangThucHien,
      tre: vanBanDon_tre,
    },
    hanhChinh: {
      daHoanThanh: hanhChinh_hoanThanh,
      chuanBiTrienKhai: hanhChinh_chuanBiTrienKhai,
      dangThucHien: hanhChinh_dangThucHien,
      daHuy: hanhChinh_daHuy,
    },
  };
  return result;
}

export async function countVanBanByStatus(
  queryParam: FilterQuery<VanBanDocument>
) {
  let result: any = {};
  result.working =
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.VanBanDon,
      tinhTrangXuLyVanBanDon: TinhTrangXuLyVanBanDon.DangGiaiQuyet,
    })) +
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.HanhChinh,
      tinhTrangXuLyVanBanHanhChinh: TinhTrangXuLyVanBanHanhChinh.DangThucHien,
    }));

  result.completed =
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.VanBanDon,
      tinhTrangXuLyVanBanDon: TinhTrangXuLyVanBanDon.DaGiaiQuyet,
    })) +
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.HanhChinh,
      tinhTrangXuLyVanBanHanhChinh: TinhTrangXuLyVanBanHanhChinh.DaHoanThanh,
    }));

  result.late =
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.VanBanDon,
      tinhTrangXuLyVanBanDon: TinhTrangXuLyVanBanDon.Tre,
    })) +
    (await countVanBan({
      ...queryParam,
      loaiVanBan: LoaiVanBan.HanhChinh,
      tinhTrangXuLyVanBanHanhChinh: TinhTrangXuLyVanBanHanhChinh.DangThucHien,
    }));

  return result;
}

// export async function findVanBanLate(query) {

// }
