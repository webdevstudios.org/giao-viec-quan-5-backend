export interface iResponsePayload<T> {
  message: string;
  data: T;
  total?: number;
  pageNo: number;
  size: number;
}
