import crypto from "crypto";
import { AddWorksheetOptions, Workbook } from "exceljs";

const styleColumn = (sheet: any) => {
  sheet.columns.forEach((column: any) => {
    column.alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    column.font = {
      name: "Arial",
      size: 12,
    };
  });
};

const styleFirstRow = (sheet: any) => {
  sheet.getRow("1").eachCell((cell: any) => {
    cell.fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: "ffec99" },
    };
  });
};

// const styleContentRows = (sheet) => {
//   for (let i = 2; i <= sheet.rowCount; i += 1) {
//     if (i % 2 === 0) {
//       sheet.getRow(i.toString()).eachCell((cell) => {
//         cell.fill = {
//           type: 'pattern',
//           pattern: 'solid',
//           fgColor: { argb: '99e9f2' },
//         };
//         cell.border = {
//           top: { style: 'thin' },
//           left: { style: 'thin' },
//           bottom: { style: 'thin' },
//           right: { style: 'thin' },
//         };
//       });
//     } else {
//       sheet.getRow(i.toString()).eachCell((cell) => {
//         cell.fill = {
//           type: 'pattern',
//           pattern: 'solid',
//           fgColor: { argb: 'd8f5a2' },
//         };
//         cell.border = {
//           top: { style: 'thin' },
//           left: { style: 'thin' },
//           bottom: { style: 'thin' },
//           right: { style: 'thin' },
//         };
//       });
//     }
//   }
// };

const styleSheet = async (sheet: any) => {
  styleColumn(sheet);
  styleFirstRow(sheet);
  return sheet;
};

const createRandomName = () => {
  const fileName = crypto.randomBytes(16).toString("hex");
  return `${__dirname}/${fileName}.xlsx`;
};

export const writeExcelToDisk = async (workbook: any) => {
  const fileName = createRandomName();
  await workbook.xlsx.writeFile(fileName);
  return fileName;
};

export const createSheet = async (
  data: any,
  columns: any,
  sheetName: any,
  workbook: Workbook,
  autoFilter: any,
  options?:Partial<AddWorksheetOptions> | undefined
) => {
  const sheet = workbook.addWorksheet(sheetName,options);
  sheet.autoFilter = autoFilter;
  sheet.columns = columns;
  sheet.addRows(data);
  styleSheet(sheet);
};
