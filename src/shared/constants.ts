// Put shared constants here

export const MAX_FILE_SIZE = 5;
export const UPLOAD_DESTINATION = "upload";
export const LIMIT_PER_PAGE = 10;

export const emailRegex = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})*$/;
// error string
export const paramMissingError =
  "One or more of the required parameters was missing.";

export const loginFailed = "Login failed!";

export const unauthorized = "Unauthorized";

export const accessNotAllow = "Access is not allowed!";

export const uploadFailed = "Upload failed!";

export const wrongFileType = "Wrong file type";

export const fileNotFound = "File not found";

export const notFound = "Not found";

export const badRequest = "Bad request";

export const invalidInput = "Invalid input";

export const Quan5Enums = {
  LoaiDon: ["Khiếu nại", "Tố cáo", "Phản ảnh"],
  TinhTrangXuLyVanBanDon: ["Đang thực hiện", "Đã giải quyết", "Trễ", "Đã hủy"],
  TinhTrangXuLyVanBanHanhChinh: [
    "Chuẩn bị triển khai",
    "Đang thực hiện",
    "Đã hoàn thành",
    "Đã hủy",
  ],
  LoaiVanBan: ["Văn bản hành chính", "Văn bản đơn"],
};

export const LoaiDon = {
  KhieuNai: Quan5Enums.LoaiDon[0],
  ToCao: Quan5Enums.LoaiDon[1],
  PhanAnh: Quan5Enums.LoaiDon[2],
};
export const LoaiVanBan = {
  HanhChinh: Quan5Enums.LoaiVanBan[0],
  VanBanDon: Quan5Enums.LoaiVanBan[1],
};

export const TinhTrangXuLyVanBanDon = {
  DangGiaiQuyet: Quan5Enums.TinhTrangXuLyVanBanDon[0],
  DaGiaiQuyet: Quan5Enums.TinhTrangXuLyVanBanDon[1],
  Tre: Quan5Enums.TinhTrangXuLyVanBanDon[2],
  DaHuy: Quan5Enums.TinhTrangXuLyVanBanDon[3],
  [Quan5Enums.TinhTrangXuLyVanBanDon[0]]: "DangThucHien",
  [Quan5Enums.TinhTrangXuLyVanBanDon[1]]: "DaGiaiQuyet",
  [Quan5Enums.TinhTrangXuLyVanBanDon[2]]: "Tre",
  [Quan5Enums.TinhTrangXuLyVanBanDon[3]]: "DaHuy",
};

export const TinhTrangXuLyVanBanHanhChinh = {
  ChuanBiTrienKhai: Quan5Enums.TinhTrangXuLyVanBanHanhChinh[0],
  DangThucHien: Quan5Enums.TinhTrangXuLyVanBanHanhChinh[1],
  DaHoanThanh: Quan5Enums.TinhTrangXuLyVanBanHanhChinh[2],
  DaHuy: Quan5Enums.TinhTrangXuLyVanBanHanhChinh[3],
  [Quan5Enums.TinhTrangXuLyVanBanHanhChinh[0]]: "ChuanBiTrienKhai",
  [Quan5Enums.TinhTrangXuLyVanBanHanhChinh[1]]: "DangThucHien",
  [Quan5Enums.TinhTrangXuLyVanBanHanhChinh[2]]: "DaHoanThanh",
  [Quan5Enums.TinhTrangXuLyVanBanHanhChinh[3]]: "DaHuy",
};

export const countdownDeadline = 86400000 * 3; //2 days;

export const oneDayTimestamp = 86400000;
